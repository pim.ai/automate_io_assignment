const express = require('express')
const bodyParser = require('body-parser')
//Import routes
const routes = require('./Routes')
var events = require('events');
global.myEventEmitter = new events.EventEmitter();



const MongoDB = require('./MongoManager.js')
const Collections = require('./Collections/Base.js')

var on_collection_load = function() {
  console.log("Initiating collection sync and Mongo DB Collnection !");
  global.mongo_con = new MongoDB({
    url: "localhost:8085",
    database: "default",
    collection_array: my_collections.all_collections,
    sync_collections: true,
    over_write_collections: false
  })
  let payload = global.my_collections.form_payload("files", {
    "name": "test_name",
    "parent_dir": 'Root',
    "owner_id": "Anonymous",
    "created_on": new Date()
  })
}
global.myEventEmitter.on('init_collection_sync', on_collection_load);
global.my_collections = new Collections()




const app = express()

app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())

//Use the routes here.
app.use('/dms/v1', routes)

//Setting the port, on which we need to run the server.
const port = 8080

//This would be the Basic GET Api route. By default this will be called.
app.get('/', (req, res) => res.send({
  server_metadata: 'I am online!'
}))

//This line is responsiible for starting the API server.
//Make sure all the Express dependent imports included before you start this server.
app.listen(port, () => console.log(`API Server listening on port ${port}.`))
