const fs = require('fs')

class CollectionBase {
  constructor() {
    this.all_collections = []
    this.master = {}
    this.parser()
  }

  parser() {
    let dirname = "./Collections"
    fs.readdir(dirname, (err, filenames) => {
      if (err) {
        onError(err);
        return;
      }
      let items_processed = 0
      let json_files = []
      for (let filename of filenames) {
        if (filename.split(".")[filename.split(".").length - 1] == "json") {
          json_files.push(filename)
        }
      }
      json_files.forEach((filename) => {
        fs.readFile(dirname + "/" + filename, 'utf-8', (err, content) => {
          items_processed++;
          if (err) {
            console.log(err);
            return;
          }
          if (content != "") {
            content = JSON.parse(content)
            console.log("Reading collection ", content.name);
            this.master[content.name] = content
            this.all_collections.push(content.name)
          }
          if (items_processed == json_files.length) {
            global.myEventEmitter.emit("init_collection_sync")
          }
        });

      });
    });
  }

  __uuid__(short = false) {
    let s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    if(short){

      return s4() + s4() + '-' + s4() + '-' + s4();
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  form_payload(collection, payload) {
    if (!this.all_collections.includes(collection)) {
      console.log(` Given Collection : ${collection} does not exist !`);
      return undefined
    }
    let collection_meta = this.master[collection]
    let final_payload = {}
    payload['created_on'] = payload['created_on'] ? payload['created_on'] : new Date()
    final_payload[collection_meta.pk] = payload[collection_meta.pk] ? payload[collection_meta.pk] : `${collection_meta.pk_prefix}${this.__uuid__(true)}`
    for(let key of Object.keys(payload)){
      if(collection_meta.keys.includes(key)){
        final_payload[key] = payload[key]
      }
    }
    return final_payload
  }



}

module.exports = CollectionBase
