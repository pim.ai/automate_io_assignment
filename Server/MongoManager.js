class MongoDB {

  /*
  Initiate Mongo DB instance and return for further use.
  config content :
    url : url of the database (withouot "mongodb:")
    database : name of the database
    overwrite_database : overwrite the database if exists
    verbose : more number of logs
    collection_array : array of collections to consider
    sync_collections : flag to write default collections
    over_write_collections : if true existing collection will be overwritten.
   */
  constructor(config) {
    console.log("Initiating MongoDB client !");
    this.mongo = require('mongodb').MongoClient;
    this.url = `mongodb://${config.url}`
    this.database = config.database
    this.collection_array = config.collection_array
    this.active_clinet = undefined
    this.mongo.connect(this.url, (err, client) => {
      if (err) throw err;
      this.active_clinet = client
      // console.log(db.listCollections());
      this.__sync_collections__(config.sync_collections, config.over_write_collections)
    });
  }

  __sync_collections__(sync, overwrite) {
    if (!sync) {
      console.log("Ignoring collection Syncing. Quiting the process");
      return
    }

    const db = this.active_clinet.db(this.database)
    db.listCollections({},{nameOnly:true}).toArray((err, collInfos) => {
      let available_collections = []
      for(let av_collection of collInfos){
        available_collections.push(av_collection.name)
      }
      console.log(available_collections);
      for (let collection of this.collection_array) {
        if (available_collections.includes(collection)) {
          if (overwrite) {
            db.dropCollection(collection, {}, (err, result) => {
              console.log(`Deleting Collection ${collection}`,result);
              db.createCollection(collection, function(err, res) {

                if (err) throw err;
                console.log(`${collection} Collection created!`);
              });
            })
          }else {
            console.log(`Collection ${collection} is already available. Since overwrite is false, ignoring creation !`);
          }
        }else {
          db.createCollection(collection, function(err, res) {

            if (err) throw err;
            console.log(`${collection} Collection created!`);
          });
        }

      }
    })
  }

  write_to_collection (collection,payload) {
    let final_payload = global.my_collections.form_payload (collection, payload)
    const db = this.active_clinet.db(this.database)
    return new Promise((resolve, reject) => {
      db.collection(collection).insertOne(final_payload, {}, (err, objects) => {
        if (err) reject(err)
          else resolve(objects)
      });
    });
  }

  fetch_collection (collection) {
    console.log("in fetch collection !!");
    return new Promise((resolve, reject) => {
      const db = this.active_clinet.db(this.database)
      db.collection(collection).find().toArray( (err, objects) => {
        if (err) reject(err)
          else resolve(objects)
      });
    });
  }

  search_collection (collection, query) {
    return new Promise((resolve, reject) => {
      const db = this.active_clinet.db(this.database)
      db.collection(collection).find(query).toArray( (err, objects) => {
        if (err) reject(err)
        else resolve(objects)
      });
    });

  }


}
module.exports = MongoDB
