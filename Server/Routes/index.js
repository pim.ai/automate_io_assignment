const express = require('express')
const router = express.Router()

//This function will be executed everytime API call is made to this server.
router.use(async (req, res, next) => {
  try {
    let temp_date = new Date()
    console.log(temp_date.toLocaleString(), req.method, req.url);
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', '*');
    //If authenticated let him go to next.
  } catch {

    console.log(x);
  }
  next();
  //else
  // res.send(404)
})

//Imoprting SimpleGet Route
const read = require ('./read')
const write = require ('./write')
const create_user = require ('./create_user')
const fetch = require ('./fetch')

//Create Route with simple_get as URL
router.use("/read",read)
router.use("/write",write)
router.use("/fetch",fetch)
router.use("/create_user",create_user)

module.exports = router
