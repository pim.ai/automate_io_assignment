const express = require('express')
const router = express.Router()
const fun = require('./fun')

router.post("/", fun)

module.exports = router
