module.exports = async (req, res) => {
  //Assign req.body where posted body parameters are captured here.
  //Data can be used at other places to access parameters.
  let data = req.body;//Here you can fetch payload posted.
  data['error'] = false
  try {
    console.log(data)
    let user_payload = data.user_payload
    let final_user_payload = global.my_collections.form_payload("users",user_payload)
    let dir_payload = {
      "name":"root",
      "parent_dir":'root',
      "owner_id":final_user_payload.user_id,
      "created_on":new Date()
    }
    dir_payload = global.my_collections.form_payload("directories",dir_payload)
    final_user_payload['root_dir'] = dir_payload.dir_id
    let data_user_result = await global.mongo_con.write_to_collection("users",final_user_payload)
    let data_dir_result = await global.mongo_con.write_to_collection("directories",dir_payload)
    data['user_result'] = data_user_result
    data['dir_result'] = data_dir_result
    data['status'] = 'ok'
  } catch (e) {
    //A best practice to include try catch in your function to avoid death of server due to exceptions.
    //This will evel help to show error on UI and logs.
    console.log(e);
    data['error'] = true
    data['message'] = e.toString()
  }
  res.send(data)
}
