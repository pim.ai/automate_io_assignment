module.exports = async (req, res) => {
  //Assign req.body where posted body parameters are captured here.
  //Data can be used at other places to access parameters.
  let data = req.body;//Here you can fetch payload posted.
  data['error'] = false
  try {
    console.log(data)
    if(data.collection == undefined){
      throw "Collection not found !"
    }
    if(data.payload == undefined){
      throw "Payload not available !"
    }
    data.payload.created_on = data.payload.created_on ? data.payload.created_on : new Date()
    let query_result = await global.mongo_con.write_to_collection(data.collection,data.payload)
    data['result'] = query_result
    data['status'] = 'ok'
  } catch (e) {
    //A best practice to include try catch in your function to avoid death of server due to exceptions.
    //This will evel help to show error on UI and logs.
    console.log(e);
    data['error'] = true
    data['message'] = e.toString()
  }
  res.send(data)
}
