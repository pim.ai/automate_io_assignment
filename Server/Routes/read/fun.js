module.exports = async (req, res) => {
  //Assign req.query where query parameters are captured here.
  //Data can be used at other places to access parameters.
  let data = req.query;
  data['error'] = false
  try {
    console.log(data)
    data['status'] = 'ok'
  } catch (e) {
    //A best practice to include try catch in your function to avoid death of server due to exceptions.
    //This will evel help to show error on UI and logs.
    console.log(e);
    data['error'] = true
    data['message'] = e.toString()
  }
  res.send(data)
}
