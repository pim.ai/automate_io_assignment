module.exports = async (req, res) => {
  //Assign req.body where posted body parameters are captured here.
  //Data can be used at other places to access parameters.
  let data = req.body;//Here you can fetch payload posted.
  data['error'] = false
  try {

    if(data.collection == undefined){
      throw "Collection not found !"
    }
    if(data.query == undefined){
      throw "Query not found. If there is no query send empty Object."
    }
    let query_result = undefined
    if(Object.keys(data.query).length == 0){
      query_result = await global.mongo_con.fetch_collection(data.collection,data.query)
    }else {
      query_result = await global.mongo_con.search_collection(data.collection,data.query)

    }

    data['result'] = query_result
    data['status'] = 'ok'
  } catch (e) {
    //A best practice to include try catch in your function to avoid death of server due to exceptions.
    //This will evel help to show error on UI and logs.
    console.log(e);
    data['error'] = true
    data['message'] = e.toString()
  }
  res.send(data)
}
